#define _CRT_SECURE_NO_WARNINGS
#define N 40
#define K 3
#include <stdio.h>
#include <string.h>
#include <time.h>
int main()
{
	int size;
	printf("Enter size of parols: ");
	scanf("%i", &size);
	int i, j, CapitalLetter, SmallLetter, Number, count;
	char s[256];
	srand(time(0));
	for (i = 0; i < N; ++i) {
		CapitalLetter = 0; SmallLetter = 0;  Number = 0;
		while (CapitalLetter == 0 || SmallLetter == 0 || Number == 0) {
			CapitalLetter = 0;  SmallLetter = 0; Number = 0;
			for (j = 0; j<size; ++j) {
				count = rand() % 3;
				if (count == 0) {
					s[j] = rand() % 26 + 'A';
					CapitalLetter = 1;
				}
				if (count == 1) {
					s[j] = rand() % 26 + 'a';
					SmallLetter = 1;
				}
				if (count == 2) {
					s[j] = rand() % 10 + '0';
					Number = 1;
				}
			}
		}
		s[size] = '\0';
		printf("%s", s);
		if ((i+1) % K == 0)
			printf("\n");
		else
			printf("\t");
	}
	return 0;
}