#include <stdio.h>
#include <conio.h>
int main()
{
	char parol[256] = "Qwerty123";
	char s[256] = "";
	int i = 0;
	char ch;
	printf("Enter the parol: ");
	while ((ch = _getch()) != '\r') {
		if (ch != '\b') {
			putchar('*');
			s[i] = ch;
			++i;
		}
		else {
			putchar('\b');
			putchar(' ');
			putchar('\b');
			--i;
			s[i] = "";
		}
	}
	printf("\n");
	int count = 1;
	for (i = 0; i < strlen(parol); ++i)
		if (s[i] != parol[i])
			count = 0;
	if (count == 1 && strlen(s) == strlen(parol))
		printf("Access granted!");
	else
		printf("Access denied!");
	return 0;
}